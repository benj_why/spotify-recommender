﻿using Spotify_Recommender_Extension;

Console.WriteLine("Welcome to the Spotify New Music Recommender");

Authoriser authoriser = new Authoriser();
Task _ = authoriser.requestClientCredentialAccessToken();

{
    bool exitMenu = false;

    while (!exitMenu)
    {
        Console.WriteLine("Options Menu");
        Console.WriteLine("1. Recommender Standard ");
        Console.WriteLine("2. Exit");

        Console.Write("Enter your choice (1,2): ");
        string userInput = Console.ReadLine();

        switch (userInput)
        {
            case "1":
                Console.WriteLine("You selected Option 1"); 
                Recommender recommender = new Recommender();
                Task recTest = recommender.recommendTest(authoriser.AccessToken);
                break;
            case "2":
                Console.WriteLine("Exiting the menu...");
                exitMenu = true;
                break;
            default:
                Console.WriteLine("Invalid choice. Please try again.");
                break;
        }
    }
}