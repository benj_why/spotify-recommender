﻿using System;
using static System.Net.WebRequestMethods;
using System.Net.Http.Headers;


namespace Spotify_Recommender_Extension
{
	public class Recommender : SpotifyCommunicator
	{
       
		public Recommender()
		{
		}

		public async Task recommendTest(AccessToken accessToken, int numberOfTracks, List<string> genreSeeds)
		{
            Console.WriteLine("beginning of rec test");

            // Set the authorization header with the access token
            spotifyHttpClient.BaseAddress = new Uri("https://api.spotify.com/v1/");
            spotifyHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue($"{accessToken.tokenType}", $"{accessToken.tokenString}");

            // Construct the request URL with parameters
            string requestUrl = $"?seed_genres={string.Join(",", genreSeeds)}";
            requestUrl += $"&limit={numberOfTracks}";


            // Send the GET request and receive the response
            HttpResponseMessage response = await spotifyHttpClient.GetAsync(requestUrl);
            Console.WriteLine("after async process started");


            // Check if the request was successful
            if (response.IsSuccessStatusCode)
            {
                // Read the response content as a string
                string responseBody = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseBody);
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                // Request failed, print the status code
                Console.WriteLine($"Request failed with status code: {response.StatusCode}");
            }
        }
	}
}

