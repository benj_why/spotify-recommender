﻿using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace Spotify_Recommender_Extension
{
	public class Authoriser : SpotifyCommunicator
	{
        private const string clientID = "f86aae654a88464387ce4970c0a1b6d1";
        private const string clientSecret = "39766ae3655c48439cd2361f7b7d670a";
        private AccessToken accessToken = new();

        public AccessToken AccessToken => accessToken;

        public Authoriser()
		{
		}


        //Creates HTTP POST request to generate access token, assigns values to access token object
        public async Task requestClientCredentialAccessToken()
        {
            using var request = new HttpRequestMessage(new HttpMethod("POST"), "https://accounts.spotify.com/api/token");
            request.Content = new StringContent($"grant_type=client_credentials&client_id={clientID}&client_secret={clientSecret}");
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

            var response = await spotifyHttpClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                // Parse the response content 
                dynamic responseContent = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                accessToken.tokenString = responseContent.access_token;
                accessToken.tokenType = responseContent.token_type;
                accessToken.expires = responseContent.expires_in;
            }
            else
            {
                // Request failed, print the status code
                Console.WriteLine($"Request failed with status code: {response.StatusCode}");
            }
        }

        public async Task requestPKCEAuthorisation()
        {

        }

        public async Task refreshAccessToken()
        {

        }

    }
}

