Spotify Recommender Project


The general concept behind this is to utilise the Spotify developer API to give users more influence/control over recommended music, rather than the typical black-box recommending that the app does.

This will allow for playlists curated based on multiple artists, and other factors like 'danceability', a metric used by Spotify but traditionally not available to the public.


Planned features



1. Recommendations based completely on user input (incl artists and other parameters)

List information to user about different inputs and what they mean
Recommender should be polymorphic for multiple use cases


(Eventually want to develop a UI to make this easier - use a form)

2. Recommendations based on users top artists / genres

3. Recommendations based on existing saved user playlists

4. Ability to save these recommendations as a named playlist to the user's account

5. Ability to regenerate recommendations with added bias to certain metrics / artists

6. Ability to play selected recommended songs

6. User can remove songs from a recommended playlist, then have the app generate however many more are necessary to fill the playlist to the amount requested

Tasks To Do

1. Add token refresher to client credentials

2. Add PKCE authorisation

3. Generalise a request builder method

4. Change from CLI to CVM - use forms for setting recommendation parameters

5. Validation for artists input for recommender
